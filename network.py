from graphviz import Graph
from graphviz import Digraph

h = Graph('hello', format='svg', engine='sfdp')

graphdata = { "Fortran" : ["ALGOL"],
          "ALGOL" : ["Simula"],
          "Lisp" : ["Smalltalk"],
          "Simula" : ["Smalltalk"],
          "Prolog" : []
        }

def generate_edges(graph):
    edges = []
    for node in graph:
        for neighbour in graph[node]:
            edges.append((node, neighbour))

    return edges        

def place_edges(graph):
    edges = []
    for node in graph:
        for neighbour in graph[node]:
             h.edge(node, neighbour)

#GV API:    dot.node('A', 'King Arthur')
#           dot.edge('B', 'L', constraint='false')
#place_edges(graphdata)
#h.render('/home/ascheinert/HelloWorld', view=True)  

influences={"Smalltalk": ["Lisp", "Simula", "Simula67"]}

influences["ALGOL"] = ["Fortran"]

influences["Simula"] = ["ALGOL"]

influences["Scheme"] = ["Lisp"]

d = Digraph('G', format='svg')
d.attr(rankdir='LR', size='8,5')

def makeSubGraphs(influencesDict):
    with d.subgraph() as s:        
        for x in influencesDict.values():
            for y in x:
                s.node(y) 

def manual():
    with d.subgraph() as s:
  #  s.attr(rank='same')
        s.node('A1','Fortran')
        s.node('A2','Lisp')

    with d.subgraph() as s:
 #   s.attr(rank='same')
        s.node('A3','ALGOL')
        s.node('A4','Smalltalk')
        s.node('A5','Simula')
        s.node('A6','Scheme')

with d.subgraph() as s:
  #  s.attr(rank='same')
        s.node("Fortran1954","Fortran")
        s.node("C1972","C")

#manual()        

#d.edges(['node1Anode2A'])
d.edge('Fortran1954','C1972')
d.edge('Fortran1954','C1972')

def buildEdges(plKv):
    for x in plKv["infList"]:
        d.edge(plKv["name"]plKv["inception"],x["name"]x["inception"])

def buildNodes(plList):
    for x in plList:
        with d.subgraph() as s:
            s.node(plKv["name"]plKv["inception"],plKv["name"])            





#d.edges(['A1A3', 'A3A5', 'A5A4', 'A2A4', 'A2A6'])

#makeSubGraphs(influences)

d.render('/home/ascheinert/HelloWorld')  

#print(generate_edges(graphdata))



print("done!") 