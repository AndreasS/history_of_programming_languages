import requests
import json

def getQueryInfluencedList(plQWikiData):
    return """
    SELECT
        ?item ?itemLabel
        ?inceptionQ ?inceptionQLabel
    WHERE 
    {
        ?item wdt:P737 wd:"""+plQWikiData+""" .
        ?item wdt:P571 ?inceptionQ
        SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
    }
    """  

## P571 inception
## P737  influenced by
## P3966 programming paradigm 
def getAllbyPropertyWithInception():
    return """
    SELECT
        ?item ?itemLabel
        ?value ?valueLabel
        ?inceptionQ ?inceptionQLabel
    WHERE 
    {
        ?item wdt:P3966 ?value .     
        ?item wdt:P571 ?inceptionQ
        SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
    }
    Limit 10
    """

#print(getQueryUrl("Q235086"))

def makeRequest(query):
    url = 'https://query.wikidata.org/sparql'
    r = requests.get(url, params = {'format': 'json', 'query': query})
    return r.json()

data1 = makeRequest(getAllbyPropertyWithInception())["results"]

#with open('data.json', 'w') as outfile:
#    json.dump(data1, outfile)

print("done writing!")

data = data1["bindings"]

#pretty print
#print(json.dumps(data, indent=4))

def getResultsInfluence(jsonResultDict):   
   kv=dict(influencedBy=jsonResultDict["influencedByLabel"]["value"], influencedWikidata=jsonResultDict["influencedBy"]["value"])
   wikidataQString=kv["influencedWikidata"]
   kv["influencedWikidata"]=wikidataQString[wikidataQString.index("Q"):]
   return kv

def getResultsProgLang(jsonResultDict):   
   kv=dict(name=jsonResultDict["itemLabel"]["value"], qWikidata=jsonResultDict["item"]["value"])
   wikidataQString=kv["qWikidata"]
   kv["qWikidata"]=wikidataQString[wikidataQString.index("Q"):]
   kv["inception"]=jsonResultDict["inceptionQLabel"]["value"][:4]
   return kv   

#print(list(map(getResults,)))

result_map_list=list(map(getResultsProgLang,data))

#result_list_item=list(map(lambda x : x["item"],result_map))

print(result_map_list[0])


php_results=makeRequest(getQueryInfluencedList(result_map_list[0]["qWikidata"]))["results"]["bindings"]

#print(php_results)

print(getResultsProgLang(php_results[0]))

empty=makeRequest(getQueryInfluencedList("Q83303"))["results"]["bindings"]

print(empty)

#print(len(result_list_item))

#no_dupes=list(dict.fromkeys(result_list_item))

#print(len(no_dupes))

#print(no_dupes)


