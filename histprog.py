from graphviz import Graph
from graphviz import Digraph
import requests
import json
import time

def buildEdges(nameInceptionParent, infList):
    for x in infList:
        d.edge(nameInceptionParent,x["name"]+x["inception"])

def buildNodes(plList):
    for x in plList:
        with d.subgraph() as s:
            s.node(x["name"]+x["inception"],x["name"]+" - "+x["inception"])

def makeRequest(query):
    print("make request!")
    url = 'https://query.wikidata.org/sparql'
    r = requests.get(url, params = {'format': 'json', 'query': query})
    return r.json()

def getQueryInfluencedList(plQWikiData):
    return """
    SELECT
        ?item ?itemLabel
        ?inceptionQ ?inceptionQLabel
    WHERE 
    {
        ?item wdt:P737 wd:"""+plQWikiData+""" .
        ?item wdt:P571 ?inceptionQ
        SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
    }
    """  

## P571 inception
## P737  influenced by
## P3966 programming paradigm 
##     
def getAllbyPropertyWithInception():
    return """
    SELECT
        ?item ?itemLabel
        ?value ?valueLabel
        ?inceptionQ ?inceptionQLabel
    WHERE 
    {
        ?item wdt:P3966 ?value .     
        ?item wdt:P571 ?inceptionQ
        SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
    }
    Limit 200
    """

def getResultsProgLang(jsonResultDict):   
   kv=dict(name=jsonResultDict["itemLabel"]["value"], qWikidata=jsonResultDict["item"]["value"])
   wikidataQString=kv["qWikidata"]
   kv["qWikidata"]=wikidataQString[wikidataQString.index("Q"):]
   kv["inception"]=jsonResultDict["inceptionQLabel"]["value"][:4]
   return kv 

# 
data = makeRequest(getAllbyPropertyWithInception())["results"]["bindings"]

result_map_list=list(map(getResultsProgLang,data))

#php_results=makeRequest(getQueryInfluencedList(result_map_list[0]["qWikidata"]))["results"]["bindings"]

print(result_map_list[0])

#print(php_results)

d = Digraph('G', format='svg')
d.attr(rankdir='LR', size='8,5')

buildNodes(result_map_list)

def buildNodesAndEdgesForChilds(plNameInception, plQWikiData):
    for x in makeRequest(getQueryInfluencedList(plQWikiData))["results"]["bindings"]:   
        childKv=getResultsProgLang(x)
        #print(childKv)
        d.node(childKv["name"]+childKv["inception"],childKv["name"]+" - "+childKv["inception"])
        d.edge(plNameInception,childKv["name"]+childKv["inception"])

for x in result_map_list:
    print("query "+x["name"]+x["inception"]+x["qWikidata"])
    # limit request rate so the wikidata web service won't cut us off ratelimit
    time.sleep(1) 
    buildNodesAndEdgesForChilds(x["name"]+x["inception"],x["qWikidata"])

d.render('/home/ascheinert/HistProgLang')  

print("done")