# history_of_programming_languages

- original source, now through wayback since its offline these days:  https://web.archive.org/web/20130203044822/http://oreilly.com/news/graphics/prog_lang_poster.pdf

## Oreily original

- Starting languages: LISP, Forth, Fortran, APL, Prolog, B-0 ( COBOL), CPL (C), Smalltalk , Simula , ALGOL, ISWIM, SNOBOL, BASIC, 

##  Python wikidata SPQRL/JSON API example

-  https://janakiev.com/blog/wikidata-mayors/

## wikidata data model

- Programming langues examples: Smalltalk https://www.wikidata.org/wiki/Q235086  

**P737  influenced by**

**P571   inception**

## TODO

- Layout and weight, more horizontal than vertical
- layout respecting time line
- Color arrows
- filter/group 
- dynamic filters: data sources, org poster, wikipedia, wikidata
